package main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"web","controllers"})
public class Application {
    public static void main(String[] args) {
        try {
            SpringApplication.run(Application.class, args);
            System.out.println("server is up!");
        } catch (Exception e) {
            System.out.println("server failed to start!");
        }
    }
}
